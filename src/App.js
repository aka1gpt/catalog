import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getUserType } from './actions/dataActions'

import './App.css'
import LoadingPage from './components/loadingPage'
import LoginPage from './components/loginPage'
import Dashboard from './components/dashboard'

function App() {
  const authenticationData = useSelector(state => state.dataReducer.authenticationData)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getUserType())
  }, [])

  useEffect(() => {

  }, [authenticationData])

  return (
    <div className='App'>
      {
        authenticationData.isLoggedIn === 'loading' &&
        <LoadingPage />
      }
      {
        authenticationData.isLoggedIn === true &&
        <Dashboard />
      }
      {
        authenticationData.isLoggedIn === false &&
        <LoginPage />
      }
    </div>
  )
}


export default App