import {
    authenticationUrl, 
    authenticationParam,
    backEndUrl
} from "../config"

const axios = require('axios')

export const updateTenant = (id) => {
    return{
        type: 'UPDATE_SELECTED_TENANT',
        payload: id
    }
}

export const updateName = (name) => {
    return{
        type: 'UPDATE_SELECTED_EVENT',
        payload: name
    }
}

export const getUserType = () => {
    return (dispatch) => {
        axios.get(authenticationUrl, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_USER_TYPE',
                    payload: {
                        userType: response.data.userType,
                        isLoggedIn: true
                    }
                })
            })
            .catch(error => {
                console.log(error);
                dispatch({
                    type: 'UPDATE_USER_TYPE',
                    payload: {
                        userType: 'unknown',
                        isLoggedIn: false
                    }
                })
            })
    }
}
export const getAllEvents = () => {
    return (dispatch) => {
        axios.get(`${backEndUrl}/event/home/`, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_ALL_EVENTS',
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    }
}

export const getSummaryTables = (id) => {
    return (dispatch) => {
        axios.get(`${backEndUrl}/event/tenant/${id}/type/COMPUTED`, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_COMPUTED_EVENTS',
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    }
}

export const getTransactionalTables = (id) => {
    return (dispatch) => {
        axios.get(`${backEndUrl}/event/tenant/${id}/type/TRANSACTIONAL`, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_TRANSACTIONAL_EVENTS',
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    }
}


export const getClickStreamTables = (id) => {
    return (dispatch) => {
        axios.get(`${backEndUrl}/event/tenant/${id}/type/BEHAVIORAL`, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_BEHAVIORAL_EVENTS',
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    }

    
}



export const getSelectedEventMeta = (id,name) => {
    return (dispatch) => {
        axios.get(`${backEndUrl}/event/tenant/${id}/name/${name}`, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_SELECTED_EVENT_META',
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    }

    
}


export const getSelectedFields = (id,name) => {
    return (dispatch) => {
        axios.get(`${backEndUrl}/eventfield/tenant/${id}/name/${name}`, {
            params: authenticationParam
        })
            .then(response => {
                dispatch({
                    type: 'UPDATE_SELECTED_FIELDS',
                    payload: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })
    }

}