import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'


import './index.scss'
import NavBar from './navBar'
import LeftSection from './leftSection'
import RightSection from './rightSection'
import { getAllEvents } from '../../actions/dataActions'







export default function Dashboard() {
    const dispatch = useDispatch()
    const allEvents = useSelector((state) => state.dataReducer.allEvents)
    useEffect(() => {
        dispatch(getAllEvents())
    },[])

    

    return (
        <div className='dashboard'>
            <NavBar />
            <div className='body'>
                <LeftSection />
                <RightSection />
            </div>
        </div>
    )
}