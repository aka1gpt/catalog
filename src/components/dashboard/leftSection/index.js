import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'

import './index.scss'
import { updateTenant,getClickStreamTables,getSummaryTables,getTransactionalTables,updateName } from '../../../actions/dataActions'
import Summary from './summary'
import Transactional from './transactional'
import ClickStream from './clickStream'


export default function LeftSection() {
    const dispatch = useDispatch()
    const handleOnClick = (e) => {
        dispatch(updateTenant(e.target.value))
        dispatch(getClickStreamTables(e.target.value))
        dispatch(getSummaryTables(e.target.value))
        dispatch(getTransactionalTables(e.target.value))
        dispatch(updateName(""))

    }



    useEffect(() => {
            dispatch(getClickStreamTables(1))
            dispatch(getSummaryTables(1))
            dispatch(getTransactionalTables(1))
        },[]
    )
    return (
        
        <div className='left-section'>
            <div className = "dropdown-container">
                <p>Select Website</p>
                <select onChange={handleOnClick}>
                <option value={1}>naukri</option>
                <option value={2}>naukri-gulf</option>
                <option value={3}>first-naukri</option>
                <option value={4}>job-hai</option>
                <option value={5}>bigshyft</option>
                <option value={6}>99acres</option>
                <option value={7}>iimjobs</option>
                <option value={8}>hirist</option>
            </select>
            </div>
            

            <p className='card-name'>Summary Tables</p>
            <div className='card'>
                <Summary />
            </div>

            <p className='card-name'>Transactional Tables</p>
            <div className='card'>
                <Transactional />
            </div>

            <p className='card-name'>Click Stream Tables</p>
            <div className='card'>
                <ClickStream />
            </div>
        </div>
    )
}