import React, { useEffect } from 'react'
import { useSelector,useDispatch } from 'react-redux'


import { updateName,getSelectedEventMeta, getSelectedFields } from '../../../actions/dataActions'

export default function Summary() {
    const computedEvents = useSelector((state) => state.dataReducer.computedEvents)
    const selectedTenant = useSelector((state) => state.dataReducer.selectedTenant)
    const selectedEvent = useSelector((state) => state.dataReducer.selectedEvent)

    const dispatch = useDispatch()
    const handleOnClick = (e) => {
        dispatch(updateName(e))
        dispatch(getSelectedEventMeta(selectedTenant,e))
        dispatch(getSelectedFields(selectedTenant,e))
    }




    useEffect(() => {
        
        }
    )
    return (
        <>
        {
            computedEvents.map((item) => 
            <div className = "line" onClick = {() => handleOnClick(item.name)}>
                <div className = "dot"></div>
                
                    <p className = {item.name === selectedEvent ? "active" : "inactive"}>{item.name}</p>
                
            </div>
            )
        }
        </>
        
    )
}