import React from 'react'

import './index.scss'
import SearchBar from './searchBar'

export default function NavBar() {
    return (
        <div className='nav-bar'>
            <p className='title'>Naurki Data Catalogue</p>
            <SearchBar />
        </div>
    )
    
}