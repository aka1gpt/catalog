import React, { useEffect, useRef, useState } from 'react'
import { useSelector,useDispatch } from 'react-redux'

import './searchBar.scss'
import { updateName,getSelectedEventMeta, getSelectedFields } from '../../../actions/dataActions'


export default function SearchBar() {

    const [groupedEventsArray, setGroupedEventsArray] = useState([[]])

    const [searchTerm, setSearchTerm] = useState('')
    const [suggestionsArr, setSuggestionsArr] = useState([])
    const allEvents = useSelector((state) => state.dataReducer.allEvents)
    const selectedTenant = useSelector((state) => state.dataReducer.selectedTenant)

    const selectedEvent = useSelector((state) => state.dataReducer.selectedEvent)
    const wrapperRef = useRef(null)
    const dispatch = useDispatch()

    const handleOnclick = (e) => {
        if (wrapperRef.current && !wrapperRef.current.contains(e.target)) {
            setSuggestionsArr([])
        }
    }

    const handleTypeWord = (e) => {
        setSearchTerm(e.target.value)
        setSuggestionsArr(groupedEventsArray[selectedTenant - 1].filter(name => name.toLowerCase().includes(e.target.value.toLowerCase())))
    }

    const handleSelectSuggestion = (e) => {
        setSearchTerm(e)
        dispatch(updateName(e))
        dispatch(getSelectedEventMeta(selectedTenant,e))
        dispatch(getSelectedFields(selectedTenant,e))
        setSuggestionsArr([])
    }

    

    useEffect(() => {
        window.addEventListener('click', handleOnclick)
        return () => {
            window.removeEventListener('click', handleOnclick)
        }
    }, [])

    

    useEffect(() => {
            let a1 = []
            let a2 = []
            let a3 = []
            let a4 = []
            let a5 = []
            let a6 = []
            let a7 = []
            let a8 = []

            allEvents.forEach((e) => {
                if (e.tenantId == 1){
                    a1.push(e.name)
                }
                else if (e.tenantId == 2){
                    a2.push(e.name)
                }
                else if (e.tenantId == 3){
                    a3.push(e.name)
                }
                else if (e.tenantId == 4){
                    a4.push(e.name)
                }
                else if (e.tenantId == 5){
                    a5.push(e.name)
                }
                else if (e.tenantId == 6){
                    a6.push(e.name)
                }
                else if (e.tenantId == 7){
                    a7.push(e.name)
                }
                else if (e.tenantId == 8){
                    a8.push(e.name)
                }
                
            })
            setGroupedEventsArray([a1,a2,a3,a4,a5,a6,a7,a8])

    },[allEvents])

    return (
        <div className='search-bar-container' ref={wrapperRef}>
            <input type='text' placeholder='Search tables....' spellCheck={false} value={searchTerm} onChange={handleTypeWord}></input>
            {
                searchTerm !== '' && suggestionsArr.length > 0 &&
                <div className='suggestions-container'>
                    {
                        suggestionsArr.map(suggestion => <div className='suggestion' onClick={() => handleSelectSuggestion(suggestion)}>{suggestion}</div>)
                    }
                </div>

            }
        </div>
    )
}