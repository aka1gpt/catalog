import React, { useState } from 'react'
import { useSelector } from 'react-redux'

import './index.scss'
import Metatable from './metatable'
import Fieldstable from './fieldstable'
import Modal from './modal'

export default function RightSection() {

    const [isModalOpen, setModalOpen] = useState(false)
    const selectedEvent = useSelector((state) => state.dataReducer.selectedEvent)
    const fields = useSelector((state) => state.dataReducer.selectedFields)

    const closeModal = () => {
        setModalOpen(false)
    }

    return (
        <div className='right-section'>
            {
                (selectedEvent === "" || fields === JSON.stringify({})) ? 
                <div className = "empty-event">
                    <p>Click on a table or search it to view it's meta</p>
                </div> :
                <div>
                    <Metatable />
                    <Fieldstable />
                </div>
            }
            {
                isModalOpen && <Modal closeModal={closeModal} />
            }
        </div>
    )
}