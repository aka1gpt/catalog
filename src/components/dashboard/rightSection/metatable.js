import React, { useState } from 'react'
import { useSelector } from 'react-redux'

import "./metatable.scss"
export default function Metatable() {
    const event = useSelector((state) => state.dataReducer.selectedEventMeta[0])

    return(
        <div className = "table-container">
            <table>
                <tr>
                        <td>Tenant</td>
                        <td>{event.tenantId}</td>
                  </tr>
                  <tr>
                        <td>Table Name</td>
                        <td>{event.name}</td>
                  </tr>
                  <tr>
                        <td>Table Type</td>
                        <td>{event.type}</td>
                  </tr>
                  <tr>
                        <td>Ingestion Statergy</td>
                        <td>{event.ingestionStrategy}</td>
                  </tr>

                  <tr>
                        <td>Inception Date</td>
                        <td>{event.inceptionDate}</td>
                  </tr>
                  <tr>
                        <td>Description</td>
                        <td>{event.description}</td>
                  </tr>
                  <tr>
                        <td>DB Type</td>
                        <td>{event.dbType}</td>
                  </tr>
                  <tr>
                        <td>DB Name</td>
                        <td>{event.dbName}</td>
                  </tr>
                  <tr>
                        <td>DB Instance</td>
                        <td>{event.dbInstance}</td>
                  </tr>
                  <tr>
                        <td>DB Table</td>
                        <td>{event.dbTable}</td>
                  </tr>
                  <tr>
                        <td>Rollup Column</td>
                        <td>{event.rollupColumn}</td>
                  </tr>
                  <tr>
                        <td>Ordering Column</td>
                        <td>{event.orderingColumn}</td>
                  </tr>
                  <tr>
                        <td>Write Mode</td>
                        <td>{event.writeMode}</td>
                  </tr>
                  <tr>
                        <td>Session Type</td>
                        <td>{event.sessionType}</td>
                  </tr>
                  <tr>
                        <td>Refresh Frequency</td>
                        <td>{event.refreshFrequency}</td>
                  </tr>
                  <tr>
                        <td>Data available till</td>
                        <td>{event.lastSyncTime}</td>
                  </tr>
                  <tr>
                        <td>Is Compacted</td>
                        <td>{(event.enableCompaction === "1")?"True":"False"}</td>
                  </tr>
                  <tr>
                        <td>Partition Type</td>
                        <td>{event.partitionType}</td>
                  </tr>
                  <tr>
                        <td>Has PII</td>
                        <td>{event.hasPII}</td>
                  </tr>
                  {/* <tr>
                        <td>Created On</td>
                        <td>{event.createdon}</td>
                  </tr>
                  <tr>
                        <td>Updated On</td>
                        <td>{event.updatedon}</td>
                  </tr>
                  <tr>
                        <td>Schema Version</td>
                        
                        <td>{event.schemaVersion}</td>
                  </tr> */}            
            </table>
        </div>
    )    
}
