import React from 'react'
import { AiFillCloseCircle } from 'react-icons/ai'

import './modal.scss'

export default function Modal(props) {
    return (
        <div className='modal-container'>
            <div className='modal'>
                <AiFillCloseCircle onClick={props.closeModal} />
            </div>
        </div>
    )
}