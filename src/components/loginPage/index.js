import React from 'react'
import { CgMicrosoft } from 'react-icons/cg'

import './index.scss'
import { loginUrl, authenticationParam, frontEndUrl } from '../../config'

const axios = require('axios')

export default function LoginPage() {

    const handleLoginClick = () => {
        axios.get(loginUrl, {
            params: {
                'success-url': frontEndUrl,
                'failure-url': frontEndUrl,
                clientId: 'infoedge',
                ...authenticationParam
            }
        })
            .then(response => {
                window.location.href = response.data.link
            })
            .catch(error => {
                console.log(error);
            })
    }

    return (
        <div className='login-page'>
            <p>You are not Logged In</p>
            <button onClick={handleLoginClick}>Click here to Login<CgMicrosoft /></button>
        </div>
    )
}