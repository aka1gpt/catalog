import Cookies from 'js-cookie'

export const backEndUrl = "http://localhost:9091"
export const frontEndUrl = "http://localhost:3000/catalog"
// export const backEndUrl = "http://10.120.0.125:8091"
// export const frontEndUrl = "http://datalake.infoedge.com/catalog"

export const loginUrl = `${backEndUrl}/authentication/microsoft/login`
export const authenticationUrl = `${backEndUrl}/authentication/success`

export const authenticationParam = {
    token: '90a1e95dba0d3d9c11e3f220cc4f7879',
    // nauk_at: Cookies.get('nauk_at')
}