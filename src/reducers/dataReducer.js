const initialState = {
    authenticationData: {
        userType: 'unknown',
        isLoggedIn: true
    },
    allEvents : [],
    selectedTenant : 1,
    transactionalEvents: [],
    behavioralEvents: [],
    computedEvents: [],
    selectedEvent: "",
    selectedEventMeta: [{}],
    selectedFields: {}
}

const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case "UPDATE_USER_TYPE": return { ...state, userType: action.payload }
        case "UPDATE_ALL_EVENTS": return { ...state, allEvents : action.payload }
        case "UPDATE_SELECTED_TENANT": return { ...state, selectedTenant : action.payload }
        case "UPDATE_COMPUTED_EVENTS": return { ...state, computedEvents : action.payload }
        case "UPDATE_TRANSACTIONAL_EVENTS": return { ...state, transactionalEvents : action.payload }
        case "UPDATE_BEHAVIORAL_EVENTS": return { ...state, behavioralEvents : action.payload }
        case "UPDATE_SELECTED_EVENT": return { ...state, selectedEvent : action.payload }
        case "UPDATE_SELECTED_EVENT_META": return { ...state, selectedEventMeta : action.payload }
        case "UPDATE_SELECTED_FIELDS": return { ...state, selectedFields : action.payload }
        default: return state;
    }
}
export default dataReducer